module Main where

import Control.Concurrent
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Loops
import Data.IORef
import Data.Time
import Data.String.Conversions
import System.Directory
import System.Environment
import System.Exit
import System.IO
import System.Process

fileChanged :: FilePath -> IORef UTCTime -> IO Bool
fileChanged filePath lastModifiedIoRef = do
    lastModified <- readIORef lastModifiedIoRef
    getResult <- try (getModificationTime filePath) :: IO (Either SomeException UTCTime)
    case getResult of
        Left _ -> pure False
        Right lm -> if lastModified /= lm
            then writeIORef lastModifiedIoRef lm >> pure True
            else pure False

watchFile :: IORef Bool -> FilePath -> IO ()
watchFile mustReload filePath = do
    lastModified <- newIORef (UTCTime { utctDay = (ModifiedJulianDay 0), utctDayTime = (secondsToDiffTime 0) })
    forever $ do
        fileDidChange <- fileChanged filePath lastModified
        if fileDidChange
            then atomicModifyIORef' mustReload (\_ -> (True, ())) -- file changed, force reload
            else pure ()
        threadDelay 100000 -- 0.1 seconds

main :: IO ()
main = do
    args <- getArgs
    filePath <- case args of
                     []     -> die "Usage: qml-watcher <file>"
                     (x:xs) -> pure x
    
    -- shared IORef between threads, only update with atomicModifyIORef!
    mustReload <- newIORef False -- TODO: use MVar to guarantee thread-safety instead

    -- check if file exists
    fileExists <- doesFileExist filePath
    if not fileExists
        then putStrLn "Warning: file does not exist"
        else pure ()
    
    -- create watcher thread
    threadId <- forkIO (watchFile mustReload filePath)

    -- run our loop
    bracket
        -- create our thread
        (forkIO (watchFile mustReload filePath))
        -- clean up our thread
        killThread
        -- watch the file forever
        (\_ -> forever $ bracket 
            -- launch the displayer
            (spawnProcess "qml-watcher-display" [filePath])
            -- terminate the displayer
            terminateProcess
            (\process -> do
                -- wait for changes
                untilM_ 
                    (threadDelay 10000)
                    (readIORef mustReload)
                -- reset ioref
                atomicModifyIORef' mustReload (\_ -> (False, ()))))
