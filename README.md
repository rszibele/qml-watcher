# qml-watcher

qml-watcher is a utility program to be used together with <a href="https://gitlab.com/rszibele/qml-watcher-display#readme">qml-watcher-display</a>
to allow for rapid application development with QML files.

You must add qml-watcher-display to the path to be able to use qml-watcher. As of now it is best used on KDE5 Plasma, as the window manager
allows you to save the position of a window, so that the created window for a qml file will always be on the same spot on your screen after
changing the qml file.

Having multiple monitors is highly recommended, as this allows you to work on your programs core logic and the gui at the same time 
while also seeing the GUI changes in real-time.

## Build Instructions

You must have stack to be able to build qml-watcher.

````
stack build
stack install
````

## Usage Instructions

````
qml-watcher /path/to/my/file.qml
````